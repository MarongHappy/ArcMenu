# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-04-15 15:12-0400\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: constants.js:65
msgid "Favorites"
msgstr ""

#: constants.js:66 menulayouts/launcher.js:331 menulayouts/runner.js:92
#: menulayouts/tweaks/tweaks.js:719
msgid "Frequent Apps"
msgstr ""

#: constants.js:67 menulayouts/az.js:127 menulayouts/az.js:153
#: menulayouts/eleven.js:158 menulayouts/eleven.js:216 menuWidgets.js:1446
msgid "All Apps"
msgstr ""

#: constants.js:68 menulayouts/insider.js:155 menulayouts/launcher.js:357
#: menulayouts/plasma.js:117 menulayouts/raven.js:55 menulayouts/raven.js:226
#: menulayouts/raven.js:275 menulayouts/tweaks/tweaks.js:717
#: menulayouts/unity.js:37 menulayouts/unity.js:282 menulayouts/unity.js:341
#: menulayouts/windows.js:380 menuWidgets.js:1112 prefs.js:3447 utils.js:280
msgid "Pinned Apps"
msgstr ""

#: constants.js:69 menulayouts/raven.js:297 menulayouts/unity.js:359
#: searchProviders/recentFiles.js:26 searchProviders/recentFiles.js:27
msgid "Recent Files"
msgstr ""

#: constants.js:229 menulayouts/mint.js:222 menulayouts/unity.js:205
#: prefs.js:343
msgid "Log Out"
msgstr ""

#: constants.js:230 menulayouts/mint.js:223 menulayouts/unity.js:206
#: prefs.js:342
msgid "Lock"
msgstr ""

#: constants.js:231 prefs.js:345
msgid "Restart"
msgstr ""

#: constants.js:232 menulayouts/mint.js:224 menulayouts/unity.js:207
#: prefs.js:344
msgid "Power Off"
msgstr ""

#: constants.js:233 prefs.js:346
msgid "Suspend"
msgstr ""

#: constants.js:234 prefs.js:347
msgid "Hybrid Sleep"
msgstr ""

#: constants.js:235 prefs.js:348
msgid "Hibernate"
msgstr ""

#: constants.js:358 prefs.js:2762
msgid "ArcMenu"
msgstr ""

#: constants.js:359
msgid "Brisk"
msgstr ""

#: constants.js:360
msgid "Whisker"
msgstr ""

#: constants.js:361
msgid "GNOME Menu"
msgstr ""

#: constants.js:362
msgid "Mint"
msgstr ""

#: constants.js:363
msgid "Budgie"
msgstr ""

#: constants.js:366
msgid "Unity"
msgstr ""

#: constants.js:367
msgid "Plasma"
msgstr ""

#: constants.js:368
msgid "tognee"
msgstr ""

#: constants.js:369
msgid "Insider"
msgstr ""

#: constants.js:370
msgid "Redmond"
msgstr ""

#: constants.js:371
msgid "Windows"
msgstr ""

#: constants.js:372
msgid "11"
msgstr ""

#: constants.js:373
msgid "a.z."
msgstr ""

#: constants.js:376
msgid "Elementary"
msgstr ""

#: constants.js:377
msgid "Chromebook"
msgstr ""

#: constants.js:380 constants.js:392
msgid "Launcher"
msgstr ""

#: constants.js:381
msgid "Runner"
msgstr ""

#: constants.js:382
msgid "GNOME Overview"
msgstr ""

#: constants.js:385
msgid "Raven"
msgstr ""

#: constants.js:389
msgid "Traditional"
msgstr ""

#: constants.js:390
msgid "Modern"
msgstr ""

#: constants.js:391
msgid "Touch"
msgstr ""

#: constants.js:393
msgid "Alternative"
msgstr ""

#: menuButton.js:636 menulayouts/arcmenu.js:178 menulayouts/plasma.js:167
#: menulayouts/raven.js:166 menulayouts/redmond.js:143
#: menulayouts/tognee.js:138 menulayouts/unity.js:152
#: menulayouts/windows.js:118 menuWidgets.js:1901 prefs.js:318 prefs.js:332
#: prefs.js:2557 prefs.js:3695
msgid "ArcMenu Settings"
msgstr ""

#: menuButton.js:640 prefs.js:1984
msgid "General Settings"
msgstr ""

#: menuButton.js:641
msgid "Menu Theming"
msgstr ""

#: menuButton.js:642
msgid "Change Menu Layout"
msgstr ""

#: menuButton.js:643
msgid "Layout Tweaks"
msgstr ""

#: menuButton.js:644
msgid "Customize Menu"
msgstr ""

#: menuButton.js:645 prefs.js:3446
msgid "Button Settings"
msgstr ""

#: menuButton.js:648 prefs.js:2741
msgid "About"
msgstr ""

#: menuButton.js:655
msgid "Dash to Panel Settings"
msgstr ""

#: menulayouts/arcmenu.js:178 menulayouts/baseMenuLayout.js:469
#: menulayouts/baseMenuLayout.js:471 menulayouts/mint.js:217
#: menulayouts/plasma.js:167 menulayouts/raven.js:166
#: menulayouts/redmond.js:143 menulayouts/tognee.js:138
#: menulayouts/unity.js:152 menulayouts/unity.js:200 menulayouts/windows.js:118
msgid "Software"
msgstr ""

#: menulayouts/arcmenu.js:178 menulayouts/az.js:121 menulayouts/eleven.js:150
#: menulayouts/insider.js:58 menulayouts/mint.js:213 menulayouts/plasma.js:167
#: menulayouts/raven.js:166 menulayouts/redmond.js:143
#: menulayouts/tognee.js:138 menulayouts/unity.js:152 menulayouts/whisker.js:49
#: menulayouts/windows.js:57 menulayouts/windows.js:118
msgid "Settings"
msgstr ""

#: menulayouts/arcmenu.js:178 menulayouts/plasma.js:167
#: menulayouts/raven.js:166 menulayouts/redmond.js:143
#: menulayouts/tognee.js:138 menulayouts/unity.js:152
#: menulayouts/windows.js:118
msgid "Tweaks"
msgstr ""

#: menulayouts/arcmenu.js:178 menulayouts/az.js:118 menulayouts/eleven.js:147
#: menulayouts/insider.js:55 menulayouts/mint.js:212 menulayouts/plasma.js:167
#: menulayouts/raven.js:166 menulayouts/redmond.js:143
#: menulayouts/tognee.js:138 menulayouts/unity.js:152 menulayouts/windows.js:54
#: menulayouts/windows.js:118 menuWidgets.js:1904
msgid "Terminal"
msgstr ""

#: menulayouts/arcmenu.js:178 menulayouts/plasma.js:167
#: menulayouts/raven.js:166 menulayouts/redmond.js:143
#: menulayouts/tognee.js:138 menulayouts/unity.js:152
#: menulayouts/windows.js:118 menuWidgets.js:825 prefs.js:331
msgid "Activities Overview"
msgstr ""

#: menulayouts/az.js:114 menulayouts/eleven.js:143 menulayouts/insider.js:52
#: menulayouts/mint.js:221 menulayouts/unity.js:204 menulayouts/windows.js:51
#: menuWidgets.js:1907
msgid "Files"
msgstr ""

#: menulayouts/az.js:128 menulayouts/az.js:147 menulayouts/az.js:204
#: menulayouts/eleven.js:159 menulayouts/eleven.js:206
#: menulayouts/eleven.js:262
msgid "Pinned"
msgstr ""

#: menulayouts/baseMenuLayout.js:417 menulayouts/baseMenuLayout.js:455
#: menulayouts/mint.js:210 menulayouts/unity.js:194 placeDisplay.js:336
msgid "Home"
msgstr ""

#: menulayouts/baseMenuLayout.js:417 menulayouts/mint.js:219
#: menulayouts/unity.js:195
msgid "Documents"
msgstr ""

#: menulayouts/baseMenuLayout.js:417 menulayouts/unity.js:196
msgid "Downloads"
msgstr ""

#: menulayouts/baseMenuLayout.js:417
msgid "Music"
msgstr ""

#: menulayouts/baseMenuLayout.js:417
msgid "Pictures"
msgstr ""

#: menulayouts/baseMenuLayout.js:417
msgid "Videos"
msgstr ""

#: menulayouts/baseMenuLayout.js:417 menulayouts/plasma.js:126
#: menulayouts/unity.js:202 placeDisplay.js:125 placeDisplay.js:148
#: prefs.js:324 prefs.js:340
msgid "Computer"
msgstr ""

#: menulayouts/baseMenuLayout.js:417 menulayouts/baseMenuLayout.js:463
#: menulayouts/plasma.js:289 menulayouts/windows.js:186 prefs.js:325
#: prefs.js:341
msgid "Network"
msgstr ""

#: menulayouts/eleven.js:160 menulayouts/eleven.js:182
#: menulayouts/eleven.js:266 menulayouts/windows.js:322
msgid "Frequent"
msgstr ""

#: menulayouts/launcher.js:110
msgid "More"
msgstr ""

#: menulayouts/launcher.js:141
msgid "All"
msgstr ""

#: menulayouts/launcher.js:147 menulayouts/launcher.js:329
#: menulayouts/plasma.js:122 menuWidgets.js:1404 menuWidgets.js:2799
msgid "Applications"
msgstr ""

#: menulayouts/launcher.js:278 menulayouts/launcher.js:338
msgid "Search"
msgstr ""

#: menulayouts/launcher.js:358 menuWidgets.js:2640
msgid "Search…"
msgstr ""

#: menulayouts/plasma.js:130 menuWidgets.js:1136
msgid "Leave"
msgstr ""

#: menulayouts/plasma.js:257 menulayouts/windows.js:253 prefs.js:3449
msgid "Application Shortcuts"
msgstr ""

#: menulayouts/plasma.js:261 menulayouts/windows.js:257
msgid "Places"
msgstr ""

#: menulayouts/plasma.js:273 menulayouts/tweaks/tweaks.js:662
#: menulayouts/tweaks/tweaks.js:764 menulayouts/windows.js:170
msgid "Bookmarks"
msgstr ""

#: menulayouts/plasma.js:281 menulayouts/windows.js:178
msgid "Devices"
msgstr ""

#: menulayouts/plasma.js:316 menuWidgets.js:1162
msgid "Session"
msgstr ""

#: menulayouts/plasma.js:321 menuWidgets.js:1166
msgid "System"
msgstr ""

#: menulayouts/raven.js:57 menulayouts/raven.js:233
#: menulayouts/tweaks/tweaks.js:460 menulayouts/tweaks/tweaks.js:529
#: menulayouts/tweaks/tweaks.js:696 menulayouts/tweaks/tweaks.js:720
#: menulayouts/unity.js:39 menulayouts/unity.js:287
msgid "All Programs"
msgstr ""

#: menulayouts/raven.js:277 menulayouts/unity.js:343
msgid "Shortcuts"
msgstr ""

#: menulayouts/tweaks/tweaks.js:32 menuWidgets.js:1474 menuWidgets.js:1507
#: prefs.js:1805
msgid "Back"
msgstr ""

#: menulayouts/tweaks/tweaks.js:117
msgid "Mouse Click"
msgstr ""

#: menulayouts/tweaks/tweaks.js:118
msgid "Mouse Hover"
msgstr ""

#: menulayouts/tweaks/tweaks.js:121
msgid "Category Activation"
msgstr ""

#: menulayouts/tweaks/tweaks.js:144
msgid "Round"
msgstr ""

#: menulayouts/tweaks/tweaks.js:145 prefs.js:2064 prefs.js:2065 prefs.js:2066
msgid "Square"
msgstr ""

#: menulayouts/tweaks/tweaks.js:147
msgid "Avatar Icon Shape"
msgstr ""

#: menulayouts/tweaks/tweaks.js:162 menulayouts/tweaks/tweaks.js:781
msgid "Bottom"
msgstr ""

#: menulayouts/tweaks/tweaks.js:163 menulayouts/tweaks/tweaks.js:354
#: menulayouts/tweaks/tweaks.js:782
msgid "Top"
msgstr ""

#: menulayouts/tweaks/tweaks.js:166
msgid "Searchbar Location"
msgstr ""

#: menulayouts/tweaks/tweaks.js:187
msgid "Flip Layout Horizontally"
msgstr ""

#: menulayouts/tweaks/tweaks.js:203
msgid "Disable User Avatar"
msgstr ""

#: menulayouts/tweaks/tweaks.js:220 menulayouts/tweaks/tweaks.js:257
msgid "Disable Frequent Apps"
msgstr ""

#: menulayouts/tweaks/tweaks.js:238
msgid "Show Applications Grid"
msgstr ""

#: menulayouts/tweaks/tweaks.js:271
msgid "Disable Pinned Apps"
msgstr ""

#: menulayouts/tweaks/tweaks.js:292
msgid "Activate on Hover"
msgstr ""

#: menulayouts/tweaks/tweaks.js:308
msgid "Brisk Menu Shortcuts"
msgstr ""

#: menulayouts/tweaks/tweaks.js:342
msgid "Enable Activities Overview Shortcut"
msgstr ""

#: menulayouts/tweaks/tweaks.js:355
msgid "Centered"
msgstr ""

#: menulayouts/tweaks/tweaks.js:357
msgid "Position"
msgstr ""

#: menulayouts/tweaks/tweaks.js:384
msgid "Width"
msgstr ""

#: menulayouts/tweaks/tweaks.js:407 prefs.js:1914
msgid "Height"
msgstr ""

#: menulayouts/tweaks/tweaks.js:430 prefs.js:3112
msgid "Font Size"
msgstr ""

#: menulayouts/tweaks/tweaks.js:431 prefs.js:1098
#, javascript-format
msgid "%d Default Theme Value"
msgstr ""

#: menulayouts/tweaks/tweaks.js:445
msgid "Show Frequent Apps"
msgstr ""

#: menulayouts/tweaks/tweaks.js:459 menulayouts/tweaks/tweaks.js:528
#: utils.js:263
msgid "Home Screen"
msgstr ""

#: menulayouts/tweaks/tweaks.js:462 menulayouts/tweaks/tweaks.js:531
#: menulayouts/tweaks/tweaks.js:698 menulayouts/tweaks/tweaks.js:722
msgid "Default View"
msgstr ""

#: menulayouts/tweaks/tweaks.js:476
msgid "Unity Layout Buttons"
msgstr ""

#: menulayouts/tweaks/tweaks.js:483
msgid "Button Separator Position"
msgstr ""

#: menulayouts/tweaks/tweaks.js:501 menulayouts/tweaks/tweaks.js:591
msgid "Adjust the position of the separator in the button panel"
msgstr ""

#: menulayouts/tweaks/tweaks.js:514 menulayouts/tweaks/tweaks.js:604
msgid "Separator Position"
msgstr ""

#: menulayouts/tweaks/tweaks.js:542 prefs.js:633 prefs.js:659
msgid "Left"
msgstr ""

#: menulayouts/tweaks/tweaks.js:543 prefs.js:635 prefs.js:661
msgid "Right"
msgstr ""

#: menulayouts/tweaks/tweaks.js:545
msgid "Position on Monitor"
msgstr ""

#: menulayouts/tweaks/tweaks.js:566
msgid "Mint Layout Shortcuts"
msgstr ""

#: menulayouts/tweaks/tweaks.js:573
msgid "Shortcut Separator Position"
msgstr ""

#: menulayouts/tweaks/tweaks.js:631 menulayouts/tweaks/tweaks.js:739
msgid "Extra Shortcuts"
msgstr ""

#: menulayouts/tweaks/tweaks.js:648 menulayouts/tweaks/tweaks.js:750
msgid "External Devices"
msgstr ""

#: menulayouts/tweaks/tweaks.js:685
msgid "Nothing Yet!"
msgstr ""

#: menulayouts/tweaks/tweaks.js:695 menulayouts/tweaks/tweaks.js:718
msgid "Categories List"
msgstr ""

#: menulayouts/tweaks/tweaks.js:772
msgid "Extra Categories Quick Links"
msgstr ""

#: menulayouts/tweaks/tweaks.js:773
msgid ""
"Display quick links of extra categories on the home page\n"
"See Customize Menu -> Extra Categories"
msgstr ""

#: menulayouts/tweaks/tweaks.js:784
msgid "Quick Links Location"
msgstr ""

#: menulayouts/tweaks/tweaks.js:817
msgid "Enable Weather Widget"
msgstr ""

#: menulayouts/tweaks/tweaks.js:831
msgid "Enable Clock Widget"
msgstr ""

#: menuWidgets.js:107 searchProviders/openWindows.js:27
msgid "Open Windows"
msgstr ""

#: menuWidgets.js:124
msgid "New Window"
msgstr ""

#: menuWidgets.js:137
msgid "Launch using Integrated Graphics Card"
msgstr ""

#: menuWidgets.js:138
msgid "Launch using Discrete Graphics Card"
msgstr ""

#: menuWidgets.js:165
msgid "Delete Desktop Shortcut"
msgstr ""

#: menuWidgets.js:178
msgid "Create Desktop Shortcut"
msgstr ""

#: menuWidgets.js:199
msgid "Remove from Favorites"
msgstr ""

#: menuWidgets.js:206
msgid "Add to Favorites"
msgstr ""

#: menuWidgets.js:228 menuWidgets.js:290
msgid "Unpin from ArcMenu"
msgstr ""

#: menuWidgets.js:241
msgid "Pin to ArcMenu"
msgstr ""

#: menuWidgets.js:253
msgid "Show Details"
msgstr ""

#: menuWidgets.js:272
msgid "Open Folder Location"
msgstr ""

#: menuWidgets.js:1099
msgid "Configure Runner"
msgstr ""

#: menuWidgets.js:1124
msgid "Extras"
msgstr ""

#: menuWidgets.js:1231
msgid "Categories"
msgstr ""

#: menuWidgets.js:1551
msgid "All Applications"
msgstr ""

#: menuWidgets.js:2202
msgid "New"
msgstr ""

#: menuWidgets.js:2961
msgid "Add world clocks…"
msgstr ""

#: menuWidgets.js:2962
msgid "World Clocks"
msgstr ""

#: menuWidgets.js:3244
msgid "Loading…"
msgstr ""

#: menuWidgets.js:3254
msgid "Go online for weather information"
msgstr ""

#: menuWidgets.js:3256
msgid "Weather information is currently unavailable"
msgstr ""

#: menuWidgets.js:3266
msgid "Weather"
msgstr ""

#: menuWidgets.js:3268
msgid "Select weather location…"
msgstr ""

#: placeDisplay.js:49
#, javascript-format
msgid "Failed to launch “%s”"
msgstr ""

#: placeDisplay.js:64
#, javascript-format
msgid "Failed to mount volume for “%s”"
msgstr ""

#: placeDisplay.js:210
#, javascript-format
msgid "Ejecting drive “%s” failed:"
msgstr ""

#: prefs.js:34 prefs.js:56
msgid "Add More Apps"
msgstr ""

#: prefs.js:45
msgid "Add Default User Directories"
msgstr ""

#: prefs.js:121
msgid "Add Custom Shortcut"
msgstr ""

#: prefs.js:307
msgid "Add to your Pinned Apps"
msgstr ""

#: prefs.js:309
msgid "Change Selected Pinned App"
msgstr ""

#: prefs.js:311
msgid "Select Application Shortcuts"
msgstr ""

#: prefs.js:313
msgid "Select Directory Shortcuts"
msgstr ""

#: prefs.js:326
msgid "Recent"
msgstr ""

#: prefs.js:333
msgid "Run Command..."
msgstr ""

#: prefs.js:334
msgid "Show All Applications"
msgstr ""

#: prefs.js:451
#, javascript-format
msgid "%s has been pinned to ArcMenu"
msgstr ""

#: prefs.js:463
#, javascript-format
msgid "%s has been unpinned from ArcMenu"
msgstr ""

#: prefs.js:496
msgid "Add a Custom Shortcut"
msgstr ""

#: prefs.js:499
msgid "Edit Pinned App"
msgstr ""

#: prefs.js:501
msgid "Edit Shortcut"
msgstr ""

#: prefs.js:511
msgid "Title"
msgstr ""

#: prefs.js:522 prefs.js:1052 prefs.js:1165
msgid "Icon"
msgstr ""

#: prefs.js:532 prefs.js:1472
msgid "Browse..."
msgstr ""

#: prefs.js:538 prefs.js:1477
msgid "Select an Icon"
msgstr ""

#: prefs.js:567
msgid "Command"
msgstr ""

#: prefs.js:570
msgid "Shortcut Path"
msgstr ""

#: prefs.js:580 prefs.js:999 prefs.js:1790
msgid "Apply"
msgstr ""

#: prefs.js:580
msgid "Add"
msgstr ""

#: prefs.js:605
msgid "General"
msgstr ""

#: prefs.js:612
msgid "Display Options"
msgstr ""

#: prefs.js:625
msgid "Show Activities Button"
msgstr ""

#: prefs.js:634 prefs.js:660
msgid "Center"
msgstr ""

#: prefs.js:637 prefs.js:1121
msgid "Position in Panel"
msgstr ""

#: prefs.js:667
msgid "Menu Alignment"
msgstr ""

#: prefs.js:686
msgid "Display ArcMenu on all Panels"
msgstr ""

#: prefs.js:687
msgid "Dash to Panel extension required"
msgstr ""

#: prefs.js:699
msgid "Hotkey Options"
msgstr ""

#: prefs.js:702
msgid "Standalone Runner Menu"
msgstr ""

#: prefs.js:745
msgid "Enable a standalone Runner menu"
msgstr ""

#: prefs.js:760
msgid "Open on Primary Monitor"
msgstr ""

#: prefs.js:767
msgid "None"
msgstr ""

#: prefs.js:768
msgid "Left Super Key"
msgstr ""

#: prefs.js:769
msgid "Custom Hotkey"
msgstr ""

#: prefs.js:772
msgid "Menu Hotkey"
msgstr ""

#: prefs.js:772
msgid "Runner Hotkey"
msgstr ""

#: prefs.js:785
msgid "Modify Hotkey"
msgstr ""

#: prefs.js:790
msgid "Current Hotkey"
msgstr ""

#: prefs.js:874
msgid "Set Custom Hotkey"
msgstr ""

#: prefs.js:899
msgid "Choose Modifiers"
msgstr ""

#: prefs.js:909
msgid "Ctrl"
msgstr ""

#: prefs.js:913
msgid "Super"
msgstr ""

#: prefs.js:917
msgid "Shift"
msgstr ""

#: prefs.js:921
msgid "Alt"
msgstr ""

#: prefs.js:973
msgid "Press any key"
msgstr ""

#: prefs.js:989
msgid "New Hotkey"
msgstr ""

#: prefs.js:1048
msgid "Menu Button"
msgstr ""

#: prefs.js:1053 prefs.js:1137
msgid "Text"
msgstr ""

#: prefs.js:1054
msgid "Icon and Text"
msgstr ""

#: prefs.js:1055
msgid "Text and Icon"
msgstr ""

#: prefs.js:1056
msgid "Hidden"
msgstr ""

#: prefs.js:1058
msgid "Appearance"
msgstr ""

#: prefs.js:1097
msgid "Padding"
msgstr ""

#: prefs.js:1149
msgid "Icon Settings"
msgstr ""

#: prefs.js:1152
msgid "Browse Icons"
msgstr ""

#: prefs.js:1188
msgid "Icon Size"
msgstr ""

#: prefs.js:1199
msgid "Menu Button Styling"
msgstr ""

#: prefs.js:1200 prefs.js:2983
msgid "Results may vary with third party themes"
msgstr ""

#: prefs.js:1204 prefs.js:1210 prefs.js:1216 prefs.js:3100 prefs.js:3126
#: prefs.js:3132
msgid "Foreground Color"
msgstr ""

#: prefs.js:1207 prefs.js:1210 prefs.js:3123 prefs.js:3126
msgid "Hover"
msgstr ""

#: prefs.js:1207 prefs.js:1213 prefs.js:3097 prefs.js:3123 prefs.js:3129
msgid "Background Color"
msgstr ""

#: prefs.js:1213 prefs.js:1216 prefs.js:3129 prefs.js:3132
msgid "Active"
msgstr ""

#: prefs.js:1219 prefs.js:3109
msgid "Border Radius"
msgstr ""

#: prefs.js:1222 prefs.js:3106
msgid "Border Width"
msgstr ""

#: prefs.js:1222
msgid "Background colors required if set to 0"
msgstr ""

#: prefs.js:1225 prefs.js:3103
msgid "Border Color"
msgstr ""

#: prefs.js:1369 prefs.js:1374
msgid "ArcMenu Icons"
msgstr ""

#: prefs.js:1397
msgid "Distro Icons"
msgstr ""

#: prefs.js:1433 prefs.js:1466
msgid "Custom Icon"
msgstr ""

#: prefs.js:1564
msgid "Legal disclaimer for Distro Icons"
msgstr ""

#: prefs.js:1620
msgid "Layouts"
msgstr ""

#: prefs.js:1644
msgid "Current Menu Layout"
msgstr ""

#: prefs.js:1659
msgid "Available Menu Layouts"
msgstr ""

#: prefs.js:1664 prefs.js:1818
#, javascript-format
msgid "%s Menu Layouts"
msgstr ""

#: prefs.js:1742
#, javascript-format
msgid "%s Layout Tweaks"
msgstr ""

#: prefs.js:1878
msgid "Menu Size"
msgstr ""

#: prefs.js:1934
msgid "Left-Panel Width"
msgstr ""

#: prefs.js:1935 prefs.js:1956 prefs.js:2050
msgid "Traditional Layouts"
msgstr ""

#: prefs.js:1955
msgid "Right-Panel Width"
msgstr ""

#: prefs.js:1976
msgid "Width Offset"
msgstr ""

#: prefs.js:1977
msgid "Non-Traditional Layouts"
msgstr ""

#: prefs.js:1989
msgid "Off"
msgstr ""

#: prefs.js:1990
msgid "Top Centered"
msgstr ""

#: prefs.js:1991
msgid "Bottom Centered"
msgstr ""

#: prefs.js:1993
msgid "Override Menu Location"
msgstr ""

#: prefs.js:2010
msgid "Show Application Descriptions"
msgstr ""

#: prefs.js:2017
msgid "Full Color"
msgstr ""

#: prefs.js:2018
msgid "Symbolic"
msgstr ""

#: prefs.js:2020
msgid "Category Icon Type"
msgstr ""

#: prefs.js:2021 prefs.js:2032
msgid "Some icon themes may not include selected icon type"
msgstr ""

#: prefs.js:2031
msgid "Shortcuts Icon Type"
msgstr ""

#: prefs.js:2049
msgid "Vertical Separator"
msgstr ""

#: prefs.js:2057
msgid "Icon Sizes"
msgstr ""

#: prefs.js:2058
msgid "Modify the icon size of various menu elements."
msgstr ""

#: prefs.js:2063 prefs.js:2109
msgid "Default"
msgstr ""

#: prefs.js:2064 prefs.js:2067 prefs.js:2111
msgid "Small"
msgstr ""

#: prefs.js:2065 prefs.js:2068 prefs.js:2112
msgid "Medium"
msgstr ""

#: prefs.js:2066 prefs.js:2069 prefs.js:2113
msgid "Large"
msgstr ""

#: prefs.js:2067 prefs.js:2068 prefs.js:2069
msgid "Wide"
msgstr ""

#: prefs.js:2071
msgid "Grid Icons"
msgstr ""

#: prefs.js:2080
msgid "Categories &amp; Applications"
msgstr ""

#: prefs.js:2082
msgid "Buttons"
msgstr ""

#: prefs.js:2084
msgid "Quick Links"
msgstr ""

#: prefs.js:2086 prefs.js:2547
msgid "Misc"
msgstr ""

#: prefs.js:2110
msgid "Extra Small"
msgstr ""

#: prefs.js:2114
msgid "Extra Large"
msgstr ""

#: prefs.js:2156
msgid "Disable ScrollView Fade Effects"
msgstr ""

#: prefs.js:2172
msgid "Disable Tooltips"
msgstr ""

#: prefs.js:2188
msgid "Alphabetize 'All Programs' Category"
msgstr ""

#: prefs.js:2204
msgid "Show Hidden Recent Files"
msgstr ""

#: prefs.js:2224 prefs.js:2237
msgid "Multi-Lined Labels"
msgstr ""

#: prefs.js:2224
msgid "Enable/Disable multi-lined labels on large application icon layouts."
msgstr ""

#: prefs.js:2259
msgid "Disable New Apps Tracker"
msgstr ""

#: prefs.js:2269
msgid "Clear All"
msgstr ""

#: prefs.js:2278
msgid "Clear Apps Marked 'New'"
msgstr ""

#: prefs.js:2322
msgid "Extra Search Providers"
msgstr ""

#: prefs.js:2333
msgid "Search for open windows across all workspaces"
msgstr ""

#: prefs.js:2347
msgid "Search for recent files"
msgstr ""

#: prefs.js:2355 prefs.js:3451
msgid "Search Options"
msgstr ""

#: prefs.js:2365
msgid "Show descriptions of search results"
msgstr ""

#: prefs.js:2380
msgid "Highlight search result terms"
msgstr ""

#: prefs.js:2399
msgid "Max Search Results"
msgstr ""

#: prefs.js:2554
msgid "Export or Import Settings"
msgstr ""

#: prefs.js:2564
msgid "Export or Import ArcMenu Settings"
msgstr ""

#: prefs.js:2565
msgid "Importing will overwrite current settings."
msgstr ""

#: prefs.js:2579
msgid "Import"
msgstr ""

#: prefs.js:2584
msgid "Import settings"
msgstr ""

#: prefs.js:2611
msgid "Export"
msgstr ""

#: prefs.js:2616
msgid "Export settings"
msgstr ""

#: prefs.js:2635
msgid "ArcMenu Settings Window Size"
msgstr ""

#: prefs.js:2651
msgid "Window Width"
msgstr ""

#: prefs.js:2671
msgid "Window Height"
msgstr ""

#: prefs.js:2680
msgid "Reset all ArcMenu Settings"
msgstr ""

#: prefs.js:2686
msgid "Reset all Settings"
msgstr ""

#: prefs.js:2692
msgid "Reset all settings?"
msgstr ""

#: prefs.js:2693
msgid "All ArcMenu settings will be reset to the default value."
msgstr ""

#: prefs.js:2769
msgid "Application Menu Extension for GNOME"
msgstr ""

#: prefs.js:2783
msgid "ArcMenu Version"
msgstr ""

#: prefs.js:2796
msgid "Git Commit"
msgstr ""

#: prefs.js:2809
msgid "GNOME Version"
msgstr ""

#: prefs.js:2817
msgid "OS"
msgstr ""

#: prefs.js:2840
msgid "Session Type"
msgstr ""

#: prefs.js:2857
msgid "Credits"
msgstr ""

#: prefs.js:2956
msgid "Theme"
msgstr ""

#: prefs.js:2982
msgid "Override Theme"
msgstr ""

#: prefs.js:2990
msgid "Menu Themes"
msgstr ""

#: prefs.js:3035
msgid "Current Theme"
msgstr ""

#: prefs.js:3063
msgid "Save as Theme"
msgstr ""

#: prefs.js:3092
msgid "Menu Styling"
msgstr ""

#: prefs.js:3115
msgid "Separator Color"
msgstr ""

#: prefs.js:3119
msgid "Menu Items Styling"
msgstr ""

#: prefs.js:3284
msgid "Save Theme As..."
msgstr ""

#: prefs.js:3295
msgid "Theme Name"
msgstr ""

#: prefs.js:3312
msgid "Save Theme"
msgstr ""

#: prefs.js:3331
msgid "Manage Themes"
msgstr ""

#: prefs.js:3420
msgid "Customize"
msgstr ""

#: prefs.js:3434 prefs.js:3445
msgid "Menu Settings"
msgstr ""

#: prefs.js:3448
msgid "Directory Shortcuts"
msgstr ""

#: prefs.js:3450
msgid "Power Options"
msgstr ""

#: prefs.js:3452
msgid "Extra Categories"
msgstr ""

#: prefs.js:3453
msgid "Fine-Tune"
msgstr ""

#: prefs.js:3472 prefs.js:3490
msgid "More Settings..."
msgstr ""

#: prefs.js:3486
msgid "Hide Sidebar"
msgstr ""

#: prefs.js:3500
msgid "Reset"
msgstr ""

#: prefs.js:3511
#, javascript-format
msgid "Reset all %s?"
msgstr ""

#: prefs.js:3512
#, javascript-format
msgid "All %s will be reset to the default value."
msgstr ""

#: prefs.js:3703
msgid "Invalid Shortcut"
msgstr ""

#: prefsWidgets.js:255
msgid "Modify"
msgstr ""

#: prefsWidgets.js:290
msgid "Move Up"
msgstr ""

#: prefsWidgets.js:306
msgid "Move Down"
msgstr ""

#: prefsWidgets.js:324
msgid "Delete"
msgstr ""

#: search.js:736
msgid "Searching..."
msgstr ""

#: search.js:738
msgid "No results."
msgstr ""

#: search.js:829
#, javascript-format
msgid "%d more"
msgid_plural "%d more"
msgstr[0] ""
msgstr[1] ""

#: searchProviders/openWindows.js:26
msgid "List of open windows across all workspaces"
msgstr ""

#: searchProviders/openWindows.js:39
#, javascript-format
msgid "'%s' on Workspace %d"
msgstr ""

#: searchProviders/recentFiles.js:77
#, javascript-format
msgid "Failed to open “%s”"
msgstr ""

#: utils.js:27
msgid "ArcMenu - Hibernate Error!"
msgstr ""

#: utils.js:27
msgid "System unable to hibernate."
msgstr ""

#: utils.js:38
msgid "ArcMenu - Hybrid Sleep Error!"
msgstr ""

#: utils.js:38
msgid "System unable to hybrid sleep."
msgstr ""
